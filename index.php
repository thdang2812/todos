<!DOCTYPE HTML>
<?php require('vendor/autoload.php'); ?>
<?php include 'db.php';

$page = (!isset($_GET['page']) ? 1 : (int)$_GET['page']);
$perPage = (isset($_GET['per-page']) && ((int)$_GET['per-page']) <= 50 ? (int)$_GET['per-page'] : 2);
$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

$sql = "select id, task,property, date_n, TIME_FORMAT(time_n, '%H:%i') as time_n, level from task limit ".$start." , ".$perPage." ";
$total = $db->query("select * from task")->num_rows;
$pages = ceil($total / $perPage);

$rows = $db->query($sql);
?>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>todos</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10"><h1>Todo list</h1></div>
            <div class="col-md-10 col-md-offset-1">
                <table class="table table-hover">
                    <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Add task</button>
                    <button class="btn btn-default float-right" onclick="print()">Print</button>
                    <hr>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add task</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="add.php">
                                        <div class="form-group">
                                            <label>Task</label>
                                            <input type="text" required name="task" class="form-control">
                                            <label>Property</label>
                                            <input type="text" required name="property" class="form-control">
                                            <label>Date</label>
                                            <input type="date" required name="date" class="form-control" placeholder="yyyy-mm-dd">
                                            <label>Time</label>
                                            <input type="time" required name="time" class="form-control">
                                            <label>Level</label>
                                            <input type="range" required name="level" min="0" max="5" id="customRange2" class="form-control-range">
                                        </div>
                                        <input class="btn btn-success" type="submit" name="Send" value="Send">
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Task</th>
                        <th scope="col">Property</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Level</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php while($row = $rows->fetch_assoc()): ?>
                            <th scope="row"><?php echo $row["id"] ?></th>
                            <td class="col-md-4"><?php echo $row["task"] ?></td>
                            <td><?php echo $row["property"] ?></td>
                            <td><?php echo $row["date_n"] ?></td>
                            <td><?php echo $row["time_n"] ?></td>
                            <td><?php echo $row["level"] ?></td>
                            <td><a href="update.php?id=<?php echo $row['id']; ?>" class="btn btn-info">Edit</a>
                                <a href="delete.php?id=<?php echo $row['id']; ?>" class="btn btn-danger">Delete</a></td>
                    </tr>
                    <?php endwhile; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <ul class="pagination">
                <?php for ($i = 1; $i <= $pages; $i++): ?>
                    <li style="width: 30px; text-align: center; border: 0.5px solid black; margin: 5px; border-radius: 5px"><a href="?page=<?php echo $i; ?>&per-page=<?php echo $perPage; ?>"><?php echo $i; ?></a></li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</body>
</html>