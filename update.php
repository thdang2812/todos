<!DOCTYPE HTML>
<?php include 'db.php';
$id = $_GET['id'];

$sql = "select task, property, date_n, TIME_FORMAT(time_n, '%H:%i') as time_n, level from task where id = '$id'";

$rows = $db->query($sql);
$row = $rows->fetch_assoc();

if(isset($_POST['Send'])) {
    $task = $_POST['task'];
    $property = $_POST['property'];
    $date_n = $_POST['date'];
    $time_n = $_POST['time'];
    $level = $_POST['level'];

    $updateSql = "UPDATE task SET task = '$task', `property` = '$property', `date_n` = '$date_n', `time_n` = '$time_n', `level` = '$level' WHERE (`id` = '$id')";

    $val = $db->query($sql);

    if($val) {
        header('location: index.php');
    }
}
?>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>todos</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-10"><h1>Todo list</h1></div>
        <div class="col-md-10 col-md-offset-1">
            <hr>
            <h5 class="modal-title" id="exampleModalLabel">Update task</h5>
        </div>
        <div class="modal-body">
            <form method="post" action="update.php">
                <div class="form-group">
                    <label>Task</label>
                    <input type="text" required name="task" value="<?php echo $row['task'] ?>" class="form-control">
                    <label>Property</label>
                    <input type="text" required name="property" value="<?php echo $row['property'] ?>" class="form-control">
                    <label>Date</label>
                    <input type="date" required name="date" value="<?php echo $row['date_n'] ?>" class="form-control" placeholder="yyyy-mm-dd">
                    <label>Time</label>
                    <input type="time" required name="time" value="<?php echo $row['time_n'] ?>" class="form-control">
                    <label>Level</label>
                    <input type="range" required name="level" value="<?php echo $row['level'] ?>" min="0" max="5" id="customRange2" class="form-control-range">
                </div>
                <input class="btn btn-success" type="submit" name="Send" value="Send">
                <a class="btn btn-sm" type="button" href="index.php" style="color: grey">Close</a>
            </form>
        </div>
    </div>
</div>


</body>
</html>